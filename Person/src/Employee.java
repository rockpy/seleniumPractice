
public abstract class Employee {
	public String EmployeeType =""; 
	private String name ="";
	private int Age =0;
	
	public int GetAge(){
		return this.Age;
	}
	public void SetAge(int Age)
	{
		this.Age = Age;
	}

	public String GetName()
	{
		return this.name; 
	}
	public void SetName(String name)
	{
		this.name = name;
	}
	
	public abstract int GetSalary();
	
}

class TechEmployee extends Employee{
	public TechEmployee()
	{
		this.EmployeeType = "TechEmployee"; 
	}
	public int GetSalary(){
		return 20000;
	}
}
class Manager extends Employee{
	public Manager()
	{
		this.EmployeeType = "Manager"; 
	}
	public int GetSalary(){
		return 10;		
	}
}


