package com.demo.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CheckWhetherTextboxIsEnanled {

	public static void main(String[] args) {
		WebDriver d = new FirefoxDriver();
		d.get("http://opensource.demo.orangehrmlive.com/");
		
		boolean flag = d.findElement(By.id("txtUsername")).isEnabled();
		if (flag){
			d.findElement(By.id("txtUsername")).sendKeys("Admin");
		}else{
			System.out.println("TextBox Is disabled");
		}

	}

}
