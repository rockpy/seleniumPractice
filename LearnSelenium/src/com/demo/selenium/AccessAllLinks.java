package com.demo.selenium;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class AccessAllLinks {

	public static void main(String[] args) {
		WebDriver d = new FirefoxDriver();
		d.get("http://opensource.demo.orangehrmlive.com/");
		d.manage().window().maximize();
		d.findElement(By.id("txtUsername")).sendKeys("Admin");
		d.findElement(By.id("txtPassword")).sendKeys("admin");
		d.findElement(By.id("btnLogin")).click();
		
	    List<WebElement> allLink = d.findElements(By.tagName("a"));
		String[] allLinkSize = new String[allLink.size()];
		int i = 0;
		for(WebElement e : allLink){
			allLinkSize[i]=e.getText();
			i++;
			System.out.println(e.getText());
		}
	}
}
